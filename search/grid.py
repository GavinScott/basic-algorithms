from enum import Enum
import sys

# An Enum for the characters to use when printing the grid to the screen
class CellValue(Enum):
    EMPTY = ' '
    WALL = '\033[93m#\033[0m'
    START = '\033[92mA\033[0m'
    DEST = '\033[92mB\033[0m'
    VISITED_CELL = 'o'
    PATH_CELL = '\033[94mX\033[0m'
    CHECKED_CELL = '\033[95m.\033[0m'

# A grid, which can be searched using A*
class Grid():
    def __init__(self, fname):
        self.fname = fname
        # Reads the file and builds the grid
        self.__read_file()
        self.__build_grid()

    # Returns all the cells that neighbor the given cell, filtering
    # out any that overlap with a wall or go off the edge of the map
    def get_open_neighbors(self, cell):
        neighbors = set()
        for x in range(max(1, cell[0] - 1), min(self.width - 1, cell[0] + 2)):
            for y in range(max(1, cell[1] - 1), min(self.height - 1, cell[1] + 2)):
                if (x,y) != cell and self.grid[y][x] != CellValue.WALL:
                    neighbors.add((x,y))
        return neighbors

    # Determines if the value in a cell can be overridden safely
    def __is_important(self, cell):
        return self.grid[cell[1]][cell[0]] in \
            [CellValue.WALL, CellValue.DEST, CellValue.START]

    # Sets the final path to the cells in the given list (for printing)
    def set_final_path(self, path):
        for cell in path:
            if not self.__is_important(cell):
                self.grid[cell[1]][cell[0]] = CellValue.PATH_CELL

    # Sets the cells to be visited to the ones in the given list (for printing)
    def set_to_visit(self, cells):
        for cell in self.cells:
            if cell in cells:
                if not self.__is_important(cell):
                    self.grid[cell[1]][cell[0]] = CellValue.VISITED_CELL
            elif self.grid[cell[1]][cell[0]] == CellValue.VISITED_CELL:
                self.grid[cell[1]][cell[0]] = CellValue.EMPTY

    # Sets the visited cells to the ones in the given list (for printing)
    def set_visited(self, cells):
        for cell in self.cells:
            if cell in cells:
                if not self.__is_important(cell):
                    self.grid[cell[1]][cell[0]] = CellValue.CHECKED_CELL
            elif self.grid[cell[1]][cell[0]] == CellValue.CHECKED_CELL:
                self.grid[cell[1]][cell[0]] = CellValue.EMPTY

    # Reads the grid information from the text file
    def __read_file(self):
        with open(self.fname, 'r') as f:
            # Map dimenstions
            params = [int(i) for i in f.readline().split(',')]
            self.width = params[0]
            self.height = params[1]
            # Start and destination cells
            self.start = tuple([int(i) for i in f.readline().split(',')])
            self.dest = tuple([int(i) for i in f.readline().split(',')])
            # Wall locations
            self.blocked = [[int(l) for l in line.split(',')] for line in f.readlines()]
            self.area = self.width * self.height
            self.cells = [(x, y) for x in range(self.width) for y in range(self.height)]

    # Builds the grid from the information in the text file
    def __build_grid(self):
        # Creates a grid with border walls
        self.grid = [[CellValue.WALL] * self.width]
        for row in range(self.height - 2):
            self.grid.append([CellValue.WALL] + [CellValue.EMPTY] * (self.width - 2)
                + [CellValue.WALL])
        self.grid.append([CellValue.WALL] * self.width)
        # Adds user-placed walls
        for cell in self.blocked:
            self.grid[cell[1]][cell[0]] = CellValue.WALL
        # Adds start and destination cells
        self.grid[self.start[1]][self.start[0]] = CellValue.START
        self.grid[self.dest[1]][self.dest[0]] = CellValue.DEST

    # Prints the grid to the screen
    def display(self):
        s = ''
        for row in self.grid[::-1]:
            s += '  ' + ' '.join([str(c.value) for c in row]) + '\n'
        sys.stdout.write('\r\n' + s + '\n')
        sys.stdout.flush()