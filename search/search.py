from search import grid
import time
import math
import sys
from enum import Enum

# An Enum for which search algorithm to use
class SEARCH_ALG(Enum):
    A_STAR = 'a_star'
    DIJKSTRAS = 'dijkstras'

# An object that can search a grid
class Search():
    def __init__(self, grid, algorithm):
        self.grid = grid
        self.algorithm = algorithm
        # The distance between two diagonal cells (to avoid finding it more than once)
        self.diag_distance = 1#math.sqrt(2)
        # The list of cells that have been visited
        self.visited = set()
        # The list of cells that need to be visited (starting with the initial cell)
        self.to_visit = set([self.grid.start])
        # A map from a cell to the cell that led to it
        self.history = {}
        # A map of each cell to their current score (distance from the start)
        # other than the starting cell, this is initialized to "infinity"
        # (I used the area of the grid) to ensure that the values are overridden
        # with better estimates as soon as possible
        self.cur_dist = {cell : self.grid.area for cell in self.grid.cells}
        self.cur_dist[self.grid.start] = 0
        # A map of each cell to a guess of the distance to the destination.
        # This is also initialized to infinity for all but the starting cell.
        self.guess_dist = dict(self.cur_dist)
        self.guess_dist[self.grid.start] = self.__distance(self.grid.start, self.grid.dest)

    # Estimates the shortest distance between two cells
    # (allowing only horizontal, vertical, and diagonal movement)
    def __distance(self, a, b):
        h = abs(a[1] - b[1])
        w = abs(a[0] - b[0])
        return min(w, h) * self.diag_distance + abs(w - h)

    # Performs the A* search for the shortest path.
    # If "display" is True, it shows the grid in the terminal as it goes.
    def solve(self, display_delay=None):
        # Keep track of the number of visited cells
        visited_cells = 0
        # Continue until there are no longer any cells to visit
        while len(self.to_visit):
            # Find the cell (from the to_visit list) with the shortest estimated distance
            cur = None
            for cell in self.to_visit:
                if cur == None or self.guess_dist[cell] < self.guess_dist[cur]:
                    cur = cell
            # If you found the destination, you're done! Find and return the path.
            if cur == self.grid.dest:
                return self.__get_path(cur, display_delay != None), visited_cells

            # Move this cell to the visited list so you don't check it twice
            self.to_visit.remove(cur)
            self.visited.add(cur)

            # Look through every cell that neighbors the current one
            for neighbor in self.grid.get_open_neighbors(cur):
                # If you've already checked that neighbor, skip it
                if neighbor in self.visited:
                    continue
                # Add the neighbor to the list of cells to visit later
                self.to_visit.add(neighbor)
                visited_cells += 1
                # Calculate the new distance (the known current distance + a guess
                # about the rest of the distance)
                distance = self.cur_dist[cur] + self.__distance(neighbor, cur)
                # If there's a quicker way to get to the neighbor, skip to the next cell
                if distance >= self.cur_dist[neighbor]:
                    continue
                # Keep track of how you got to this neighbor
                self.history[neighbor] = cur
                # Keep track of the distance from the start cell to this neighbor
                self.cur_dist[neighbor] = distance
                # Guess the distance from the start cell to the destination based on this path.
                # In Dijkstra's algorithm, only the known distance is used (no guessing).
                guess = 0
                if self.algorithm == SEARCH_ALG.A_STAR:
                    guess = self.__distance(neighbor, self.grid.dest)
                self.guess_dist[neighbor] = distance + guess

                # Print out current state (if in "display mode")
                if display_delay != None:
                    self.grid.set_to_visit(self.to_visit)
                    self.grid.set_visited(self.visited)
                    self.grid.display()
                    time.sleep(display_delay)

    # Finds the path from A to B by backtracking from the destination, using
    # the dictionary that recorded each cell's immediate predecessor
    def __get_path(self, cur, display):
        path = [cur]
        while cur in self.history:
            cur = self.history[cur]
            path.append(cur)
        # Print out the final path (if in "display mode")
        if display:
            self.grid.set_final_path(path)
            self.grid.display()
        return path[::-1]