from search import search
from search import grid
import argparse

# Parses and returns command line arguments
def parse_arguments():
    parser = argparse.ArgumentParser(description='Searching Demonstration')
    parser.add_argument('--alg', choices=[t.value for t in search.SEARCH_ALG],
        help='the searching algorithm to use', required=True)
    parser.add_argument('--filename', type=str, required=True,
        help='the filename of the map to be searched')
    parser.add_argument("--animate", help="toggles display/animation",
        action="store_true")
    parser.add_argument('--delay', type=float, default=0.1,
        help='the animation delay, in seconds')
    parser.add_argument("--options", action="help",
        help="shows a specific demo's command-line options")
    args, unknown = parser.parse_known_args()
    return args

# Run the search demo based on the command line arguments
def run():
    args = parse_arguments()
    delay = args.delay if args.animate else 0
    # Perform the search & show output
    path, num_visited = search.Search(
        grid.Grid(args.filename),
        search.SEARCH_ALG(args.alg)).solve(delay)
    print('Path Length:', len(path), 'Visited Nodes:', num_visited)
    if not args.animate:
        print(path)