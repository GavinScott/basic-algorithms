from enum import Enum
from multiprocessing import Process, Queue
from structures import BinaryHeap
import random

# An Enum for which sorting algorithm to use
class SORT_ALG(Enum):
    INSERTION = 'insertion'
    BUBBLE = 'bubble'
    MERGE = 'merge'
    HEAP = 'heap'
    QUICK = 'quick'
    COUNTING = 'counting'
    RADIX = 'radix'
    BUCKET = 'bucket'

# Performs an insertion sort on the data
def insertion_sort(data):
    # Evaluate all data
    # (starting from second because you compare with the previous value)
    for target in range(1, len(data)):
        # Set the target as the starting point
        check = target
        # Move the target value backwards until the value behind it is lower,
        # or until you hit the start of the list
        while data[check - 1] > data[check] and check:
            # Move the target value back one index
            data[check], data[check - 1] = data[check - 1], data[check]
            # Display the data (for demonstration purposes)
            data.display(highlight=[check - 1])
            # Continue to check the previous index
            check -= 1
    return data

# Performs a bubble sort on the data
def bubble_sort(data):
    # Set the number of unsorted indices to the full length of the list
    num_unsorted = len(data)
    # Set the initial condition (no do-while loops in Python)
    swapped = True
    # Continue until there is a pass that required no swaps
    while swapped:
        # Start the pass with no swaps
        swapped = False
        # Loop over entire list (up to the highest unsorted index)
        for i in range(1, num_unsorted):
            # Check if the target value is not higher than the one below
            if data[i - 1] > data[i]:
                # Swap the values
                data[i - 1], data[i] = data[i], data[i - 1]
                # Display the data (for demonstration purposes)
                data.display(highlight=[i],
                    highlight_bracket=range(num_unsorted, len(data)))
                # Record that at least one swap happened on this pass
                swapped = True
        # After each pass, the highest unsorted value is put in it's place,
        # so after the first pass you don't need to check the value in the
        # last index, and after the second pass you can ignore the top two, etc.
        num_unsorted -= 1
    return data

# Performs a merge sort on the data
def merge_sort(data, output_queue=None):
    # Only one item, return "sorted" list automatically
    if len(data) == 1:
        return data
    # More than one item: split the data in half
    split = int(len(data)/2)
    # Sort each half separately
    left = merge_sort(data[:split])
    right = merge_sort(data[split:])
    # Merge the two lists into one
    merged = __merge(left, right)
    # (For parallel implementations) output the result to the queue
    if output_queue != None:
        output_queue.put(merged)
    # Return the final, sorted data
    return merged

# Merges two sorted lists into one sorted list (for MergeSort)
def __merge(left, right):
    # Make a new empty list
    merged = left.new_list()
    # Go through each list, adding the lowest elements of each to
    # the new list and removing them from the originals. Eventually,
    # at least one of the original lists will no longer have any data
    while len(left) and len(right):
        # Because both lists are sorted, the next smallest value will
        # always be at index zero of one of the lists. Find and append
        # that value next, then remove it from the original list.
        if left[0] > right[0]:
            merged.append(right.pop(0))
        else:
            merged.append(left.pop(0))
    # Either both lists are empty, or one has values left
    # that will be larger than anything in "merged", so we can
    # just tack the extra on at the end. Adding the empty list
    # doesn't change anything, so we don't need to check to see
    # if either has anything left over.
    merged.extend(left)
    merged.extend(right)
    # Return the new, merged list
    return merged

# Performs a heap sort on the data
def heap_sort(data):
    # Sort the data using a heap
    return BinaryHeap(data).sort()

# Performs a quick sort on the data (recursively)
def quick_sort(data, left=0, right=None, random_pivot=False):
    # Initially, 'left' and 'right' should be either end of the data
    right = len(data) - 1 if right == None else right
    # If there's at least one element between 'left' and 'right'
    if left < right:
        # Partition the chunk of data between 'left' and 'right'
        # and return the index of the pivot point
        pndx = __partition(data, left, right, random_pivot)
        # Do Quicksort on the data that's left of the pivot
        quick_sort(data, left, pndx - 1)
        # Do Quicksort on the data that's right of the pivot
        quick_sort(data, pndx + 1, right)
    # Return the sorted data (it is already sorted in-place)
    return data

# Performs a quick sort on the data (iteratively)
def quick_sort_iterative(data, random_pivot=False):
    # Initialize 'left' and 'right' as either end of the array
    left, right = 0, len(data) - 1
    # At first, need to do the whole array (from left to right)
    todo = [(left, right)]
    # Continue until there's nothing left to do
    while len(todo):
        # Get the next section to handle from the 'todo' list
        left, right = todo.pop()
        if left < right:
            # Partition that section
            p = __partition(data, left, right, random_pivot)
            # Add the data to the left of the pivot to the 'todo' list
            todo.append((left, p - 1))
            # Add the data to the right of the pivot to the 'todo' list
            todo.append((p + 1, right))
    # Return the sorted data (it is already sorted in-place)
    return data

# Partitions a subsection of the data (for quicksort)
def __partition(data, left, right, random_pivot):
    # Choose a random pivot point
    if random_pivot:
        # Choose a pivot between 'left' and 'right'
        pndx = random.randint(left + 1, right)
        # Move the data at the pivot to the left side
        data[pndx], data[left] = data[left], data[pndx]
    # Set the pivot and the wall to the left-most data
    pndx = left
    wall = left
    # Loop through all the data (ignoring the pivot)
    for i in range(left, right + 1):
        # If the data at an index is less than the pivot, it needs to
        # be moved to the left of the wall
        if data[i] < data[pndx]:
            # Move the wall forward
            wall += 1
            # Swap the larger data with what was previously just to
            # the right of the wall
            data[i], data[wall] = data[wall], data[i]
            # Display the data (for demonstration purposes)
            if i != wall:
                data.display(highlight=[i, wall])
    # Move the pivot point to the location of the wall by swapping the
    # value at the pivot with the value just to the right of the wall
    data[pndx], data[wall] = data[wall], data[pndx]
    # Display the data (for demonstration purposes)
    if pndx != wall:
        data.display(highlight=[left, wall])
    return wall

# Runs a sorting algorithm in parallel, than merges the resulting arrays
def general_parallelize(data, func, params, num_processes):
    # Split the data so each new process will get an equal part
    split_data = [data[i::num_processes] for i in range(num_processes)]
    # Create a Queue for retrieving data from
    q = Queue()
    # Spawn and start the specified number of new processes,
    # each with a different piece of the data
    processes = []
    for i in range(num_processes):
        processes.append(Process(
            target=lambda func, params, q: q.put(func(*params)),
            args=(func, (split_data[i], *params), q)))
        processes[-1].start()
    # Merge the sorted lists as the processes return them, into
    # the final, sorted data
    merged = q.get()
    # print('merged:', merged)
    for i in range(num_processes - 1):
        next = q.get()
        # print('next:', next)
        merged = __merge(merged, next)
    # Wait for each process to close
    for i in range(num_processes):
        processes[i].join()
    # Return the final, sorted data
    return merged

# Performs a counting sort on the data
def counting_sort(data):
    # Find the largest number in the data
    max_num = max(data)
    # Initialize an array of zeroes with a length equal to
    # the largest number in the data
    count = [0 for i in range(max_num + 1)]
    # Loop over all the data, increasing the number in the count
    # array (at the index corresponding to the data element) by 1
    for num in data:
        count[num] += 1
    # Overwrite the data with the new sorted data by looping over
    # the count array and adding 'N' instances of the index, where
    # 'N' is the number in the count array at that index
    dndx = 0
    for ndx in range(max_num + 1):
        for i in range(count[ndx]):
            data[dndx] = ndx
            dndx += 1
    # Return the sorted data (already sorted in-place)
    return data

# Performs a radix sort on the data
def radix_sort(data):
    # Set the initial digit to the "1's" digit
    digit = 1
    # Set the maximum digit based on the largest value in the data
    # (1000 for numbers <= 9999, 100 for numbers <= 999, etc.)
    max_digit = 10**(len(str(max(data))) - 1)
    # Sort by each digit, starting with the "1's" digit
    while digit <= max_digit:
        # Initialize a 'count' array for each possible number (0-9),
        # with an empty list in each cell
        counts = [[] for i in range(10)]
        # Add each element of the data to the count array based on
        # the current digit
        for num in data:
            counts[int(num / digit) % 10].append(num)
        # Overwrite the values in the data array, sorted by the
        # current (and previous) digits
        ndx = 0
        for li in counts:
            for ele in li:
                data[ndx] = ele
                ndx += 1
        # Move on to the next digit
        digit *= 10
    # Return the sorted data (already sorted in place)
    return data

# Performs a bucket sort on the data
def bucket_sort(data, sub_sort_alg=quick_sort):
    # Set the maximum digit based on the largest value in the data
    # (1000 for numbers <= 9999, 100 for numbers <= 999, etc.)
    max_digit = 10**(len(str(max(data))) - 1)
    # Initialize a 'count' array for each possible number (0-9),
    # with an empty list in each cell
    counts = [data.new_list([]) for i in range(10)]
    # Add each element of the data to the count array based on
    # the maximum digit
    for num in data:
        counts[int(num / max_digit) % 10].append(num)
    # Sort each array individually
    for ndx in range(10):
        counts[ndx] = sub_sort_alg(counts[ndx])
    # Overwrite the values in the data array, sorted by the
    # current (and previous) digits
    ndx = 0
    for li in counts:
        for ele in li:
            data[ndx] = ele
            ndx += 1
    # Return the sorted data (already sorted in place)
    return data

# Chooses a sorting algorithm and sorts the data
def sort(alg, data, num_processes=1, do_display=False, do_random_pivot=False,
        do_iterative=False, do_display_final=False):
    # Run the appropriate algorithm
    alg = SORT_ALG(alg)
    args = []
    # Choose the correct function (and arguments)
    if alg == SORT_ALG.INSERTION:
        func = insertion_sort
    elif alg == SORT_ALG.BUBBLE:
        func = bubble_sort
    elif alg == SORT_ALG.MERGE:
        func = merge_sort
    elif alg == SORT_ALG.HEAP:
        func = heap_sort
    elif alg == SORT_ALG.QUICK:
        func = quick_sort_iterative if do_iterative else quick_sort
        args = [] if do_iterative else [0, None]
        args.append(do_random_pivot)
    elif alg == SORT_ALG.COUNTING:
        func = counting_sort
    elif alg == SORT_ALG.RADIX:
        func = radix_sort
    elif alg == SORT_ALG.BUCKET:
        func = bucket_sort

    # Check for specific algorithm requirements
    if alg in [SORT_ALG.COUNTING, SORT_ALG.RADIX, SORT_ALG.BUCKET] and \
            not data.only_positive_integers():
        raise Exception("This algorithm only handles positive integer data")

    # Run the function in parallel (with final merge) or serially
    if num_processes > 1:
        s_data = general_parallelize(data, func, args, num_processes)
    else:
        s_data = func(data, *args)
    # Convert the data to a Data object
    s_data = data.new_list(s_data)
    # Display the final, sorted data
    s_data.display(highlight_bracket=range(len(data)), force=do_display_final)
    # Return the sorted data
    return s_data