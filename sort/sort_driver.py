import argparse
import random
from enum import Enum
import time
import sys
import string
from sort import sort

# An Enum for the possible data types to generate & sort
class DATA_TYPE(Enum):
    STR = 'string'
    INT = 'integer'

# A wrapper for a list that handles displaying to the terminal.
# some list-inheritance code borrowed from here:
# https://stackoverflow.com/questions/25511436/python-is-is-possible-to-have-a-class-method-that-acts-on-a-slice-of-the-class
class Data(list):
    # Initializes a data object (like a list, with some other attributes)
    def __init__(self, dtype, max_val, do_display, only_positive, array=[]):
        if isinstance(array, int):
            list.__init__(self, [array])
        else:
            list.__init__(self, array)
        self.dtype = dtype
        self.max_val = max_val
        self.do_display = do_display
        self.only_positive = only_positive

    # Returns whether or not the data is only positive integers
    def only_positive_integers(self):
        return self.only_positive and self.only_integers()

    # Returns whether or not the data is only integers
    def only_integers(self):
        return DATA_TYPE(self.dtype) == DATA_TYPE.INT

    # Manually checks to see if the data is sorted
    def is_sorted(self):
        for i in range(1, len(self)):
            if self[i - 1] > self[i]:
                return False
        return True

    # Overrides the __getitem__ method inherited from the 'list'
    # class to return more Data objects instead of lists
    def __getitem__(self, arg):
        if isinstance(arg, slice):
            # Standard slice notation
            retval = super(Data, self).__getitem__(arg)
        elif isinstance(arg, int):
            # A single specific index
            return list.__getitem__(self, arg)
        else:
            # Raise an error on unknown
            raise SyntaxError("Unknown notation for list slice or index")
        return type(self)(self.dtype, self.max_val, self.do_display,
            self.only_positive, array=retval)

    # Creates a copy of this Data object with the new array
    def new_list(self, array=[]):
        return Data(self.dtype, self.max_val, self.do_display,
            self.only_positive, array=array)

    # Displays the data to the terminal
    def display(self, highlight=[], highlight_bracket=[], force=False):
        if self.do_display or force:
            s = '\n'
            indent = '  '
            if DATA_TYPE(self.dtype) == DATA_TYPE.STR:
                # Display string data
                for ndx in range(len(self)):
                    color = '\033[96m' if ndx in highlight else ''
                    s += '{}{}{}\033[0m\n'.format(indent, color, self[ndx])
            elif DATA_TYPE(self.dtype) == DATA_TYPE.INT:
                # Display integer data
                for ndx in range(len(self)):
                    num = self[ndx]
                    num_neg = abs(min(0, num))
                    neg_color = '\033[96m' if ndx in highlight else '\033[95m'
                    pos_color = '\033[96m' if ndx in highlight else '\033[92m'
                    bracket_color = '\033[94m' if ndx in highlight_bracket \
                        else '\033[93m'
                    s += '{}{}{}{}{}||{}{}\033[0m\n'.format(
                        indent,
                        neg_color,
                        ' '*(self.max_val-num_neg),
                        'O'*num_neg,
                        bracket_color,
                        pos_color,
                        'O'*max(0, num))
            else:
                raise Exception('Unsupported data type: ' + str(self.dtype))
            # Refresh the terminal
            sys.stdout.write('\r\n' + s + '\n')
            sys.stdout.flush()
            # Pause, for ease-of-understanding
            time.sleep(0.1)
            return s

# Generates a Data object based on command line input
def generate_data(args):
    data = Data(args.dtype, args.max_val, args.animate, args.only_positive)
    # Set the random seed
    random.seed(args.seed)
    # Determine the function used to generate data
    if args.dtype == DATA_TYPE.STR.value:
        # A lambda function to generate a random string
        gen_func = lambda: ''.join(random.choice(string.ascii_uppercase)
            for i in range(1, random.randint(2, args.max_val)))
    else:
        opts = range(1 if args.only_positive else -args.max_val, args.max_val + 1)
        gen_func = lambda: random.choice(
            [i for i in opts if i != 0])
    # Generate either integers or strings
    for i in range(args.length):
        data.append(gen_func())
    return data

# Parses and returns command line arguments
def parse_arguments():
    parser = argparse.ArgumentParser(description='Sorting Demonstration')
    parser.add_argument('--dtype', choices=[t.value for t in DATA_TYPE],
        help='the type of data to generate and sort', default=DATA_TYPE.INT.value)
    parser.add_argument('--alg', choices=[t.value for t in sort.SORT_ALG],
        help='the sorting algorithm to use', required=True)
    parser.add_argument('--length', type=int, default=30,
        help='the length of the list to be sorted')
    parser.add_argument('--max_val', type=int, default=30,
        help='the maximum absolute value of integers to be sorted, ' +
            'or maximum character length if sorting strings')
    parser.add_argument('--seed', type=int, default=None,
        help='the random seed to use to generate the data')
    parser.add_argument('--processes', type=int, default=1,
        help='the number of processes to spawn (for appropriate algorithms)')
    parser.add_argument("--animate", help="toggles display/animation",
        action="store_true", default=False)
    parser.add_argument("--presort", help="toggles pre-sorting the data",
        action="store_true", default=False)
    parser.add_argument("--random_pivot", help="toggles a random pivot in quicksort",
        action="store_true", default=False)
    parser.add_argument("--iterative", help="toggles iterative mode for quicksort",
        action="store_true", default=False)
    parser.add_argument("--display_final", action="store_true", default=False,
        help="toggles whether to override the animation flag and show the final data")
    parser.add_argument("--options", action="help",
        help="shows a specific demo's command-line options")
    parser.add_argument("--only_positive", action="store_true", default=False,
        help="toggles only positive integer data")

    args, unknown = parser.parse_known_args()
    return args

# Run the sorting demo based on the command line arguments
def run():
    args = parse_arguments()
    data = generate_data(args)
    if args.presort:
        data = data.new_list(sorted(data))
    s_data = sort.sort(args.alg, data,
        num_processes=args.processes,
        do_display=args.animate,
        do_random_pivot=args.random_pivot,
        do_iterative=args.iterative,
        do_display_final=args.display_final)