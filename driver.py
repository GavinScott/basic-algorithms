import argparse
from enum import Enum
from sort import sort_driver
from search import search_driver

# An Enum for which demo to run
class DEMO(Enum):
    SEARCH = 'search'
    SORT = 'sort'

# Parses and returns command line arguments
def parse_arguments():
    parser = argparse.ArgumentParser(description='Algorithm Practice')
    parser.add_argument('--demo', choices=[t.value for t in DEMO],
        help='which demo to run', required=True)
    args, unknown = parser.parse_known_args()
    return args

# Runs the demo specified by the command line
if __name__ == '__main__':
    args = parse_arguments()
    demo = DEMO(args.demo)
    if demo == DEMO.SORT:
        sort_driver.run()
    elif demo == DEMO.SEARCH:
        search_driver.run()