import math

# A binary heap, that can be either a max- or a min- heap
class BinaryHeap():
    # Instantiates a new BinaryHeap
    def __init__(self, data=[], is_max_heap=True):
        # Creates the internal array. An empty heap will still
        # have one value in the array, to simplify the math to
        # find the parents of an index
        self.__ary = [None]
        # Save the original data (not used in heap operations)
        self.orig_data = data
        # Set the intial size to zero
        self.size = 0
        # This variable determines if it's a max- or min-heap
        self.is_max_heap = is_max_heap
        # If starting with data, adds each value to make a valid heap
        for element in data:
            self.insert(element)

    # Adds a new value to the heap
    def insert(self, element):
        # Increase the size of the heap
        self.size += 1
        # If there isn't room in the array, double its size
        if self.size >= len(self.__ary):
            self.__ary.extend([None for i in range(self.size)])
        # Add the value to the bottom of the heap
        self.__ary[self.size] = element
        # Filter that value up until the heap is valid
        self.__filter_up(self.size)

    # Returns whether or not the heap is empty
    def is_empty(self):
        return self.size == 0

    # Shows the root value without changing the heap
    def peek_root(self):
        if not self.is_empty():
            return self.__ary[1]

    # Removes and returns the root value
    def pop(self):
        # Do nothing if the heap is empty
        if not self.is_empty():
            # Retrieve the root value, and move the bottom value
            # in the heap to be the new root
            value, self.__ary[1] = self.__ary[1], self.__ary[self.size]
            # Reduce the size of the heap
            self.size -= 1
            # Filter down the new root value until the heap is valid
            self.__filter_down(1)
            # Return the original root value
            return value

    # Performs a heap-sort on the data
    def sort(self):
        # Preserve the original data and size of the heap
        orig_size = self.size
        orig_data = self.__ary
        # Continue until all the data has been sorted
        while not self.is_empty():
            # Show the data (for demonstration purposes, not part of the sort)
            self.__display_midsort_data(orig_size)
            # Move the largest value to the end of the list
            self.__swap(self.size, 1)
            # Reduce the size (to ignore the last value)
            self.size -= 1
            # Fix the heap by filtering down the new root value
            self.__filter_down(1)
        # Save the sorted list and reset the heap
        data = self.__ary[1 : orig_size + 1]
        self.size = orig_size
        self.__ary = orig_data
        # Return the sorted data
        return data

    # Displays the data (for demonstration purposes)
    def __display_midsort_data(self, orig_size):
        # If not a custom "Data" object, do not display
        try:
            self.orig_data.new_list(self.__ary[1 : orig_size + 1]).display(
                highlight=[self.size],
                highlight_bracket=range(self.size, orig_size))
        except:
            pass

    # Loops through each parent-child relationship and checks if they
    # are all valid (based on the heap's type)
    def is_valid(self):
        for ndx in range(1, len(self.__ary)):
            # Get the left and right indices
            lndx = 2 * ndx
            rndx = lndx + 1
            # Return false if a parent-child relationship is invalid
            if lndx <= self.size and self.__a_above_b(lndx, ndx) or \
                    rndx <= self.size and self.__a_above_b(rndx, ndx):
                return False
        return True

    # Returns True if the value at index 'a' is "above" the value at
    # index 'b' (if it's a maximum-heap, it returns True if the value
    # at 'a' is greater than the value at 'b', and the opposite for
    # a minimum-heap)
    def __a_above_b(self, a, b):
        if self.is_max_heap:
            return self.__ary[a] > self.__ary[b]
        return self.__ary[a] < self.__ary[b]

    # Swap the values at index 'a' and 'b'
    def __swap(self, a, b):
        self.__ary[a], self.__ary[b] = self.__ary[b], self.__ary[a]

    # Filters down from the given index until it has valid children/parent
    def __filter_up(self, ndx):
        # Find the index of the parent
        pndx = math.floor(ndx / 2)
        # While the node has an invalid parent
        while self.__ary[pndx] != None and not self.__a_above_b(pndx, ndx):
            # Swap with the parent
            self.__swap(pndx, ndx)
            # Update the index to the new spot (previously the parent) and
            # find the index of the new parent (previously the grandparent)
            ndx, pndx = pndx, math.floor(ndx / 2)

    # Filters down from the given index until it has valid children/parent
    def __filter_down(self, ndx):
        # Get the index of the child that is most likely to invalidate
        # the parent-child relationship
        cndx = self.__highest_child(ndx)
        # While the node has children and the parent-child relationship
        # is invalid
        while cndx > 0 and self.__a_above_b(cndx, ndx):
            # Swap the parent with the child that violates the relationship
            self.__swap(ndx, cndx)
            # Move on to the new index (which was the childs)
            ndx = cndx
            # Refresh "cndx" with the new children (the previous grandchildren)
            cndx = self.__highest_child(ndx)

    # Returns the index of the child of the specified index that
    # should be the highest in the tree if one were to become the
    # parent (the larger value if it's a max-heap, and the smallest
    # if it's a min-heap). Returns -1 if there are no children.
    def __highest_child(self, ndx):
        # Get the indices for the left and right children
        lndx = 2 * ndx
        rndx = lndx + 1
        # If there are no children, return -1
        if lndx > self.size and rndx > self.size:
            cndx = -1
        # If there is only a left child
        elif rndx > self.size and lndx <= self.size:
            cndx = lndx
        # Compare the left and right values, return the "highest"
        # (based on whether it's a min- or max-heap)
        elif self.__a_above_b(lndx, rndx):
            cndx = lndx
        else:
            cndx = rndx
        # Return the index
        return cndx

    # Recursive function for building the indented string
    # representation of the heap
    def __get_str(self, ndx, indent=0):
        s = ''
        if ndx <= self.size:
            s += '\n' + ' . '*indent + '\033[92m{}\033[0m'.format(
                str(self.__ary[ndx]))
            s += self.__get_str(2 * ndx, indent + 1)
            s += self.__get_str(2 * ndx + 1, indent + 1)
        return s

    # Returns the indented string representation of the heap
    def __str__(self):
        return self.__get_str(1)